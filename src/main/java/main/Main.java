package main;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by medvedevyakov on 26/10/2018.
 */
public class Main {

    private static Logger log = LoggerFactory.getLogger(PigLatinServlet.class);

    public static void main(String[] args) {

        PigLatinServlet allRequestsServlet = new PigLatinServlet();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(allRequestsServlet), "/*");

        Server server = new Server(8080);
        server.setHandler(context);

        try {
            server.start();
            log.info("Server started !!!");
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
