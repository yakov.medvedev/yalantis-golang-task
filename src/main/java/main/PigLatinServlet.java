package main;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by medvedevyakov on 2019-03-04.
 */
public class PigLatinServlet extends HttpServlet {

    private static Logger log = LoggerFactory.getLogger(PigLatinServlet.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) {

//        String rezult = request.getParameter("message");
//        log.info("Parameter ----->" + rezult);
        try {
//            JSONObject jo = new JSONObject();
//            jo.put("message", rezult);

//            Enumeration<String> e = request.getHeaderNames();
//            while (e.hasMoreElements()){
//                if (e.hasMoreElements()){
//                    log.info("Header ----->" + e.nextElement() + "  " + request.getHeader(e.nextElement()));
//                    jo.put(e.nextElement(), request.getHeader(e.nextElement()));
//                }
//            }
//            response.getWriter().println(jo.toString());
            response.getWriter().println(new JSONArray().put(new JSONObject().put("name","JACS")).toString());
        } catch (IOException e) {
            log.error("", e);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doGet(req, resp);
    }
}
