package main;

import java.util.ArrayList;

/**
 * Created by medvedevyakov on 2019-03-04.
 */
public class PigLatinTranslator {

    public static String Consonant = "BCDFGHJKLMNPQRSTVXZW".toLowerCase();
    public static String Vowel = "AEIOUY".toLowerCase();
    public static String postfix = "-ay";

    public PigLatinTranslator() {
    }

    public static String translate(String input){
        if (input == null) return "";
        String[] splittedInput = input.replaceAll("[^a-zA-Z ]+","").split(" ");
        ArrayList rez = new ArrayList();

        for (String s : splittedInput){
            StringBuilder sb = new StringBuilder();
            for (String character : s.toLowerCase().split("")) {
                if (PigLatinTranslator.Vowel.contains(character) && s.startsWith(character)){
                    rez.add(startsWithVowel(s));
                    break;
                }
                if (PigLatinTranslator.Consonant.contains(character)) {
                    sb.append(character);
                } else {
                   break;
                }
            }
            String r = s.substring(sb.length()) + sb + PigLatinTranslator.postfix;
            if (!rez.contains(r)) rez.add(r);
        }
        return rez.toString();
    }

    private static String startsWithVowel(String input){
        return input + postfix;
    }
}
